var fs = require('fs');
var zlib = require('zlib');

var result = fs.createReadStream('output.txt');
result.pipe(zlib.createGzip()).pipe(fs.createWriteStream('output.txt.gz'));
console.log('File compressed');
