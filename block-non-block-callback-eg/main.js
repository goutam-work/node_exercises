var fs = require('fs');

//blocking mode
var data = fs.readFileSync('text.txt');
console.log(data.toString());

//non-blocking mode
/*fs.readFile('text.txt',function(err,data){
	if(err){
		console.log(err);
		return;
	}
	console.log(data.toString());
})
*/
console.log('program ended');