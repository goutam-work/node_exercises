var events = require ('events');
var eventEmitter = new events.EventEmitter();

var listener1 = function listener1(){
	console.log('listener1 executed.');
}
var listener2 = function listener2(){
	console.log('listener2 executed.');
}
eventEmitter.addListener('connection',listener1);
eventEmitter.addListener('connection',listener2);
var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners+"Listeners listening to connection event");
eventEmitter.emit('connection');
eventEmitter.removeListener('connection',listener1);
console.log("listener1 removed..");
eventListeners  = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners+"listener1rs listening to connection event");
eventEmitter.emit('connection');
console.log("Program Ended");